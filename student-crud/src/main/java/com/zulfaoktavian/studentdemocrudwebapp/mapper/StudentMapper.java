/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zulfaoktavian.studentdemocrudwebapp.mapper;
import com.zulfaoktavian.studentdemocrudwebapp.dto.StudentDto;
import com.zulfaoktavian.studentdemocrudwebapp.entity.Student;
/**
 *
 * @author Dell
 */
public class StudentMapper {
    //map student entity to Student Dto
    public static StudentDto mapToStudentDto(Student student){
        //Membuat dto dengan builder pattern (inject dari Lombok)
        StudentDto studentDto = StudentDto.builder()
                .id(student.getId())
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .birthDate(student.getBirthDate())
                .createdOn(student.getCreatedOn())
                .updatedOn(student.getUpdatedOn())
                .build();
        return studentDto;
    }
    //map Student Dto ke Student Entity
    public static Student mapToStudent(StudentDto studentDto){
        Student student = Student.builder()
                .id(studentDto.getId())
                .firstName(studentDto.getFirstName())
                .lastName(studentDto.getLastName())
                .birthDate(studentDto.getBirthDate())
                .createdOn(studentDto.getCreatedOn())
                .updatedOn(studentDto.getUpdatedOn())
                .build();
        return student;
}
    }