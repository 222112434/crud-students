/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.zulfaoktavian.studentdemocrudwebapp.repository;

/**
 *
 * @author Dell
 *
 */

import com.zulfaoktavian.studentdemocrudwebapp.entity.Student;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**...13 lines */
public interface StudentRepository extends JpaRepository<Student, Long>{
    // contoh method abstract baru.
    Optional<Student> findByLastName(String lastName);
}
