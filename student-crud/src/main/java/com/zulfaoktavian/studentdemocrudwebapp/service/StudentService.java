/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zulfaoktavian.studentdemocrudwebapp.service;

/**
 *
 * @author Dell
 */

import com.zulfaoktavian.studentdemocrudwebapp.dto.StudentDto;
import com.zulfaoktavian.studentdemocrudwebapp.entity.Student;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface StudentService {
    public List<StudentDto> ambilDaftarStudent();
    public void perbaruiDataStudent(StudentDto studentDto);
    public void hapusDataStudent(Long studentId);
    public void simpanDataStudent(StudentDto studentDto);
    public StudentDto cariById (Long id);
}
