/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zulfaoktavian.studentdemocrudwebapp.service;
import com.zulfaoktavian.studentdemocrudwebapp.dto.StudentDto;
import com.zulfaoktavian.studentdemocrudwebapp.entity.Student;
import com.zulfaoktavian.studentdemocrudwebapp.mapper.StudentMapper;
import com.zulfaoktavian.studentdemocrudwebapp.repository.StudentRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

/**
 *
 * @author Dell
 */
@Service
public class StudentServiceImpl implements StudentService {

    private StudentRepository studentRepository;
    
    public StudentServiceImpl(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }
        
    @Override
    public List<StudentDto> ambilDaftarStudent() {
        List<Student> students = this.studentRepository.findAll();
        //konversi obj student ke studentDto satu per satu dengan fungsi map
        List<StudentDto> studentDtos = students.stream()
                .map((student) -> (StudentMapper.mapToStudentDto(student)))
                .collect(Collectors.toList());
        return studentDtos;
    }

    @Override
    public void perbaruiDataStudent(StudentDto studentDto) {
        Student student = StudentMapper.mapToStudent(studentDto);
        this.studentRepository.save(student);
    }
    
    @Override
    public void hapusDataStudent(Long studentId) {
        this.studentRepository.deleteById(studentId);
    }
    
    @Override
    public void simpanDataStudent(StudentDto studentDto) {
        Student student = StudentMapper.mapToStudent(studentDto);
        //system.out.println(student);
        studentRepository.save(student);
    }

    public StudentDto cariById(Long id){
        Student student = studentRepository.findById(id).orElse(null);
        StudentDto studentDto = StudentMapper.mapToStudentDto(student);
        return studentDto;
    }
    

}
